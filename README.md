# www.widmann.dev

[![status-badge](https://ci.codeberg.org/api/badges/13929/status.svg)](https://ci.codeberg.org/repos/13929)

This repository contains the source code of [my personal webpage](https://www.widmann.dev).

The website is built with [Quarto](https://quarto.org/).
